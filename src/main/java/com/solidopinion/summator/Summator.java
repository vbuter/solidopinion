package com.solidopinion.summator;

import java.io.File;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicLong;

public class Summator {

    public static final int DEFAULT_BUFFER_SIZE = 512 * 1024;

    public static void main(String[] args) throws Exception {
	long t0 = System.currentTimeMillis();
	if (args.length < 1) {
	    System.out.println("Please input path to file as first argument.");
	    return;
	}
	String filename = args[0];
	File file = new File(filename);
	AtomicLong sum = new AtomicLong(0);
	ExecutorService exec = Executors.newWorkStealingPool();
	List<Future<Exception>> futures = new LinkedList<>();
	int readedBytes, readedBytesWithRem, limit;
	byte[] remainder = new byte[0];
	FileChannel fc = new RandomAccessFile(file, "r").getChannel();
	long totalBytes = 0L;
	while (totalBytes < fc.size()) {
	    ByteBuffer buffer = ByteBuffer.allocateDirect(DEFAULT_BUFFER_SIZE);
	    buffer.put(remainder);
	    readedBytes = fc.read(buffer);
	    readedBytesWithRem = remainder.length + readedBytes;
	    limit = (readedBytesWithRem / Integer.BYTES) * Integer.BYTES;
	    remainder = new byte[readedBytesWithRem % Integer.BYTES];
	    for (int i = 0; i < remainder.length; i++) {
		remainder[i] = buffer.get(limit + i);
	    }
	    totalBytes += readedBytes;
	    buffer.flip();
	    buffer.limit(limit);
	    futures.add(exec.submit(() -> {
		try {
		    long sumBuffer = 0;
		    byte[] bb = new byte[Integer.BYTES];
		    while (buffer.position() < buffer.limit()) {
			buffer.get(bb);
			long unsignedIntLE = 0;
			for (int i = 0; i < bb.length; i++) {
			    unsignedIntLE |= (bb[i] & 0x00000000000000ffL) << i;
			}
			sumBuffer += unsignedIntLE;
		    }
		    sum.addAndGet(sumBuffer);
		} catch (Exception e) {
		    return e;
		}
		return null;
	    }));
	}
	System.out.println("file read duration = " + (System.currentTimeMillis() - t0) + " ms");
	Exception e = null;
	for (Future<Exception> f : futures) {
	    e = f.get();
	    if (e != null) {
		break;
	    }
	}
	System.out.println("total duration = " + (System.currentTimeMillis() - t0) + " ms");
	if (e == null) {
	    System.out.println("sum = " + sum.get());
	} else {
	    System.out.println("Failure!");
	    e.printStackTrace();
	}
	fc.close();
	exec.shutdown();
    }

}
